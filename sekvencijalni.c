#include <stdio.h>
#include <time.h>

int main()
{
	double vrijeme = 0.0;

	clock_t pocetak = clock();
    for(int p=2;p<=1000;p++){

        int a;//slucajni broj manji od p
        int fl=1;
        int brt=0;//broj testiranja
        while(brt<3){

            a=(int)(rand()%(p-1))+1;
            int br=0;
            int pow=1;
            while(br<p-1){

                pow=pow*a;
                pow=pow%p;
                br++;

            }

            if(pow!=1){

                fl=0;
                break;

            }

            brt++;
        }

        if(fl==1){
           printf("Number %d is prime number\n",p);
        }
        else
        {
           printf("Number %d is not prime number \n",p);
        }
    }

	clock_t kraj = clock();


	vrijeme += (double)(kraj - pocetak) / CLOCKS_PER_SEC;

	printf("Execution time : %f s", vrijeme);

	return 0;
}
