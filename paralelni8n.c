#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


struct arg_struct {
    int poc;
    int kraj;
};


void *FermatTest(void *arguments){

    struct arg_struct *args = (struct arg_struct *)arguments;

    int poc = args->poc;
    int kraj=args->kraj;

    for(int p=poc;p<=kraj;p++){

        int a;
        int fl=1;
        int brt=0;
        while(brt<3){

            a=(int)(rand()%(p-1))+1;
            int br=0;
            int pow=1;

            while(br<p-1){

                pow=pow*a;
                pow=pow%p;
                br++;

            }

            if(pow!=1){

                fl=0;
                break;

            }

            brt++;
        }

        if(fl==1){
           printf("Number %d is prime number\n",p);
        }
        else
        {
           printf("Number %d is not prime number\n",p);
        }
    }

 }
 int main (int argc, char *argv[])
 {
    double vrijeme = 0.0;

	clock_t pocetak = clock();


    pthread_t thread1;
    pthread_t thread2;
    pthread_t thread3;
    pthread_t thread4;
    pthread_t thread5;
    pthread_t thread6;
    pthread_t thread7;
    pthread_t thread8;

    int rc1;
    int rc2;
    int rc3;
    int rc4;
    int rc5;
    int rc6;
    int rc7;
    int rc8;

    struct arg_struct args1;
    args1.poc = 2;
    args1.kraj = 12500;
    struct arg_struct args2;
    args2.poc = 12501;
    args2.kraj = 25000;
    struct arg_struct args3;
    args3.poc = 25001;
    args3.kraj = 37500;
    struct arg_struct args4;
    args4.poc = 37501;
    args4.kraj = 50000;
    struct arg_struct args5;
    args5.poc = 50001;
    args5.kraj = 62500;
    struct arg_struct args6;
    args6.poc = 62501;
    args6.kraj = 75000;
    struct arg_struct args7;
    args7.poc = 75001;
    args7.kraj = 87500;
    struct arg_struct args8;
    args8.poc = 87501;
    args8.kraj = 100000;

    rc1 = pthread_create(&thread1, NULL, FermatTest, (void *)&args1);
    if (rc1){
          printf("ERROR; return code from pthread_create() is %d\n", rc1);
          exit(-1);
    }

    rc2 = pthread_create(&thread2, NULL, FermatTest, (void *)&args2);
    if (rc2){
          printf("ERROR; return code from pthread_create() is %d\n", rc2);
          exit(-1);
    }
    rc3 = pthread_create(&thread3, NULL, FermatTest, (void *)&args3);
    if (rc3){
          printf("ERROR; return code from pthread_create() is %d\n", rc3);
          exit(-1);
    }

    rc4 = pthread_create(&thread4, NULL, FermatTest, (void *)&args4);
    if (rc4){
          printf("ERROR; return code from pthread_create() is %d\n", rc4);
          exit(-1);
    }

    rc5 = pthread_create(&thread5, NULL, FermatTest, (void *)&args5);
    if (rc5){
          printf("ERROR; return code from pthread_create() is %d\n", rc5);
          exit(-1);
    }

    rc6 = pthread_create(&thread6, NULL, FermatTest, (void *)&args6);
    if (rc6){
          printf("ERROR; return code from pthread_create() is %d\n", rc6);
          exit(-1);
    }
    rc7 = pthread_create(&thread7, NULL, FermatTest, (void *)&args7);
    if (rc7){
          printf("ERROR; return code from pthread_create() is %d\n", rc7);
          exit(-1);
    }

    rc8 = pthread_create(&thread4, NULL, FermatTest, (void *)&args8);
    if (rc8){
          printf("ERROR; return code from pthread_create() is %d\n", rc8);
          exit(-1);
    }

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);
    pthread_join(thread5, NULL);
    pthread_join(thread6, NULL);
    pthread_join(thread7, NULL);
    pthread_join(thread8, NULL);

    clock_t kraj = clock();


	vrijeme += (double)(kraj - pocetak) / CLOCKS_PER_SEC;

	printf("Execution time : %f s", vrijeme);


 }
