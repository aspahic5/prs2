#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


struct arg_struct {
    int poc;
    int kraj;
};


void *FermatTest(void *arguments){

    struct arg_struct *args = (struct arg_struct *)arguments;

    int poc = args->poc;
    int kraj=args->kraj;

    for(int p=poc;p<=kraj;p++){

        int a;
        int fl=1;
        int brt=0;
        while(brt<3){

            a=(int)(rand()%(p-1))+1;
            int br=0;
            int pow=1;

            while(br<p-1){

                pow=pow*a;
                pow=pow%p;
                br++;

            }

            if(pow!=1){

                fl=0;
                break;

            }

            brt++;
        }

        if(fl==1){
           printf("Number %d is prime number\n",p);
        }
        else
        {
           printf("Number %d is not prime number\n",p);
        }
    }

 }
 int main (int argc, char *argv[])
 {
    double vrijeme = 0.0;

	clock_t pocetak = clock();


    pthread_t thread1;
    pthread_t thread2;
    int rc;
    int rc2;

    struct arg_struct args1;
    args1.poc = 2;
    args1.kraj = 5000;
    struct arg_struct args2;
    args2.poc = 5001;
    args2.kraj = 10000;

    rc = pthread_create(&thread1, NULL, FermatTest, (void *)&args1);
    if (rc){
          printf("ERROR; return code from pthread_create() is %d\n", rc);
          exit(-1);
    }

    rc2 = pthread_create(&thread2, NULL, FermatTest, (void *)&args2);
    if (rc2){
          printf("ERROR; return code from pthread_create() is %d\n", rc2);
          exit(-1);
    }

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    clock_t kraj = clock();


	vrijeme += (double)(kraj - pocetak) / CLOCKS_PER_SEC;

	printf("Execution time : %f s", vrijeme);


 }
